#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add %u %s %s --verbose\n", base, lhs, rhs);
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t max_len = (lhs_len > rhs_len) ? lhs_len : rhs_len;
  size_t result_len = max_len + 1;

  char *result = (char *)malloc((result_len + 1) * sizeof(char));
  if (result == NULL) {
    debug_abort("Failed to allocate memory for result");
    return NULL;
  }

  size_t carry = 0;
  size_t i;
  for (i = 0; i < max_len; ++i) {
    size_t lhs_digit = (i < lhs_len) ? get_digit_value(lhs[lhs_len - i - 1]) : 0;
    size_t rhs_digit = (i < rhs_len) ? get_digit_value(rhs[rhs_len - i - 1]) : 0;

    size_t sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    size_t digit = sum % base;

    result[i] = to_digit(digit);
  }

  if (VERBOSE) {
    fprintf(stderr, "%s + %s = %s\n", lhs, rhs, reverse(result));
    fprintf(stderr, "add: entering function\n");
  }

  carry = 0;
  for (i = 0; i < max_len; ++i) {
    size_t lhs_digit = (i < lhs_len) ? get_digit_value(lhs[lhs_len - i - 1]) : 0;
    size_t rhs_digit = (i < rhs_len) ? get_digit_value(rhs[rhs_len - i - 1]) : 0;

    size_t sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    size_t digit = sum % base;

    result[i] = to_digit(digit);

    if (VERBOSE) {
      fprintf(stderr, "add: digit %zu digit %zu carry %zu \n", lhs_digit, rhs_digit, carry);
      fprintf(stderr, "add: result : digit %zu carry %zu\n", digit, carry);
    }
  }

  if (VERBOSE && carry == 1) {
    fprintf(stderr, "add: final carry 1\n");    
  }

  if (carry > 0) {
    result[i] = to_digit(carry);
    ++i;
  }

  result[i] = '\0';

  return reverse(result);
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub %u %s %s --verbose\n", base, lhs, rhs);
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  char *rplhs = strdup(lhs);
  char *rprhs = strdup(rhs);

  rplhs = reverse(rplhs);
  rprhs = reverse(rprhs);

  const size_t max_len = 36;
  char *result = (char *) malloc((max_len)*sizeof(char));

  unsigned int carry = 0;
  size_t i = 0;
  
  while (rplhs[i] != '\0' || rprhs[i] != '\0' || carry != 0) {
    unsigned int a = get_digit_value(rplhs[i]);
    unsigned int b = get_digit_value(rprhs[i]);
    unsigned int sum;
    if (a == -1) a = 0;
    if (b == -1) b = 0;
    if (a == 0 && b == 0 && carry == 1)
      break;

    if (a >= b + carry) {
      sum = a - b - carry;
      carry = 0;
    } else if (a < b + carry) {
      sum = base + a - b - carry;
      carry = 1;
    }

    const char digit = to_digit(sum);
    result[i] = digit;

    i += 1;
  }

  if (VERBOSE) {
    fprintf(stderr, "%s - %s = %s\n", lhs, rhs, reverse(result));
    fprintf(stderr, "sub: entering function\n");
  }

  i = 0;
  while (rplhs[i] != '\0' || rprhs[i] != '\0' || carry != 0) {
    unsigned int a = get_digit_value(rplhs[i]);
    unsigned int b = get_digit_value(rprhs[i]);
    unsigned int sum;

    if (a == -1) a = 0;
    if (b == -1) b = 0;
    if (a == 0 && b == 0 && carry == 1)
      return NULL;

    if (a >= b + carry) {
      sum = a - b - carry;
      carry = 0;
    } else if (a < b + carry) {
      sum = base + a - b - carry;
      carry = 1;
    }

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %i digit %i carry %i \n", a, b, carry);
      fprintf(stderr, "sub: result : digit %i carry %i\n", sum, carry);
    }

    i += 1;
  }


  result[i] = '\0';

  reverse(result);
  result = drop_leading_zeros(result);
  
  return result;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul %u %s %s --verbose\n", base, lhs, rhs);
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);

  size_t result_len = lhs_len + rhs_len + 1;
  char *result = (char *)malloc((result_len + 1) * sizeof(char));
  if (result == NULL) {
    debug_abort("Failed to allocate memory for result");
    return NULL;
  }

  memset(result, '0', result_len);
  result[result_len] = '\0';

for (size_t i = 0; i < lhs_len; ++i) {
    size_t carry = 0;
    size_t lhs_digit = get_digit_value(lhs[lhs_len - i - 1]);

    for (size_t j = 0; j < rhs_len; ++j) {
      size_t rhs_digit = get_digit_value(rhs[rhs_len - j - 1]);

      size_t product = lhs_digit * rhs_digit + carry + get_digit_value(result[i + j]);
      carry = product / base;
      size_t digit = product % base;

      result[i + j] = to_digit(digit);
    }

    if (carry > 0) {
      result[i + rhs_len] = to_digit(carry);
    }
  }

  reverse(result);
  result = drop_leading_zeros(result);

  if (VERBOSE) {
    fprintf(stderr, "%s * %s = %s\n", lhs, rhs, result);
    fprintf(stderr, "mul: entering function\n");
  }

  for (size_t i = 0; i < lhs_len; ++i) {
    size_t carry = 0;
    size_t lhs_digit = get_digit_value(lhs[lhs_len - i - 1]);

    for (size_t j = 0; j < rhs_len; ++j) {
      size_t rhs_digit = get_digit_value(rhs[rhs_len - j - 1]);

      size_t product = lhs_digit * rhs_digit + carry + get_digit_value(result[i + j]);
      carry = product / base;
      size_t digit = product % base;
      if (VERBOSE) {
        fprintf(stderr, "mul: digit %i digit %i carry %i \n", lhs_digit, rhs_digit, carry);
        fprintf(stderr, "mul: result : digit %i carry %i\n", digit, carry);
      }
    }
  }

  return result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
